package com.example.sergi.contadorvidamagic;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private View view;

    //Variables de vida y veneno por jugador
    public MainActivityFragment() {
    }

    private int pj1Life=20;
    private int pj2Life=20;
    private int poisonPj1=0;
    private int poisonPj2=0;
    private TextView contadorPj1;
    private TextView contadorPj2;
    private ImageView errImg;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putInt("pj1Life", pj1Life);
        savedInstanceState.putInt("pj2Life", pj2Life);
        savedInstanceState.putInt("poisonPj1", poisonPj1);
        savedInstanceState.putInt("poisonPj2", poisonPj2);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();


        if ( id == R.id.resetBtn){
                resetVal();
                Snackbar.make(view, "Reset status", Snackbar.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            pj1Life = savedInstanceState.getInt("pj1Life");
            pj2Life = savedInstanceState.getInt("pj2Life");
            poisonPj1 = savedInstanceState.getInt("poisonPj1");
            poisonPj2 = savedInstanceState.getInt("poisonPj2");
        }


        view = inflater.inflate(R.layout.fragment_main, container, false);

        Button btnPoisonP1More = view.findViewById(R.id.Pj1PoisonMore);
        Button btnPoisonP2More = view.findViewById(R.id.Pj2PoisonMore);
        Button btnPoisonP1Less = view.findViewById(R.id.Pj1PoisonLess);
        Button btnPoisonP2Less = view.findViewById(R.id.Pj2PoisonLess);

        ImageButton btnLifeP1Up = view.findViewById(R.id.Pj1LifeUp);
        ImageButton btnLifeP2Up = view.findViewById(R.id.Pj2LifeUp);
        ImageButton btnLifeP1Down = view.findViewById(R.id.Pj1LifeDown);
        ImageButton btnLifeP2Down = view.findViewById(R.id.Pj2LifeDown);

        ImageButton btnPj1aPj2 = view.findViewById(R.id.Pj1aPj2);
        ImageButton btnPj2aPj1 = view.findViewById(R.id.Pj2aPj1);

        contadorPj1 = (TextView) view.findViewById(R.id.Pj1Conter);
        contadorPj2 = (TextView) view.findViewById(R.id.Pj2Counter);

        btnPoisonP1More.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poisonPj1++;
                updateViews();
            }
        });

        btnPoisonP1Less.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                poisonPj1--;
                updateViews();
            }
        });

        btnPoisonP2Less.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                poisonPj2--;
                updateViews();
            }
        });

        btnPoisonP2More.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                poisonPj2++;
                updateViews();
            }
        });

        btnLifeP1Up.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj1Life++;
                updateViews();
            }
        });

        btnLifeP2Up.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj2Life++;
                updateViews();
            }
        });

        btnLifeP2Down.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj2Life--;
                updateViews();
            }
        });

        btnLifeP1Down.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj1Life--;
                updateViews();
            }
        });

        btnPj1aPj2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj2Life = pj1Life;
                updateViews();
            }
        });

        btnPj2aPj1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pj1Life = pj2Life;
                updateViews();
            }
        });

        updateViews();

        return view;
    }
    private void updateViews() {
        contadorPj1.setText(String.format("PS: %d / Poison:%d", pj1Life, poisonPj1));
        contadorPj2.setText(String.format("PS: %d / Poison:%d", pj2Life, poisonPj2));
    }

    private void updateErrs(){

    }

    private void resetVal(){
        pj1Life=20;
        pj2Life=20;
        poisonPj1=0;
        poisonPj2=0;
        updateViews();
    }

}

