package com.example.sergi.wikibindingofisaac;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by sergi on 9/12/17.
 */

public class SharedViewModel extends ViewModel {

    private final MutableLiveData<Item> selected = new MutableLiveData<Item>();

    public LiveData<Item> getSelected() {
        return selected;
    }

    public void select(Item item)
    {
        selected.setValue(item);
    }





}
