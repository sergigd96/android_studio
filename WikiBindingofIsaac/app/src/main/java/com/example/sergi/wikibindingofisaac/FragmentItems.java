package com.example.sergi.wikibindingofisaac;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.sergi.wikibindingofisaac.databinding.FragmentActivityListBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergi on 11/12/17.
 */

public class FragmentItems extends android.support.v4.app.Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public FragmentItems() {
    }

    private FragmentActivityListBinding binding;
    private ItemViewModel model;
    private SharedViewModel sharedModel;
    private ProgressDialog dialog;
    private String param;

    @SuppressLint("ValidFragment")
    public FragmentItems(String param) {
        this.param = param;
    }

    ArrayList<Item> items;
    private ItemAdapter adapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentItems.OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentItems.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentItems newInstance(String param1, String param2) {
        FragmentItems fragment = new FragmentItems();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void refresh()  {
        model.reload();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mListener != null) {
            mListener.onFragmentInteraction("The Wiki Of Isaac");
        }

        binding = FragmentActivityListBinding.inflate(inflater);
        View view = binding.getRoot();
        SwipeRefreshLayout swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        items = new ArrayList<>();
        adapter = new ItemAdapter(
                getContext(), R.layout.lv_items_layout, items
        );

        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        binding.lvItems.setAdapter(adapter);
        binding.lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Item itemPos = (Item) adapterView.getItemAtPosition(i);

                if (itemPos != null){
                    Intent detail = new Intent(getContext(), DetailActivity.class);
                    detail.putExtra("item", itemPos);
                    startActivity(detail);
                }
            }
        });

        model = ViewModelProviders.of(this).get(ItemViewModel.class);

        model.getItems(param).observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(@Nullable List<Item> items) {
                adapter.clear();
                adapter.addAll(items);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean seMuestra) {
                if (seMuestra) {
                    dialog.show();
                } else {
                    dialog.dismiss();
                }
            }
        });
/*
        try{
        if (model.getItems(param).getValue().isEmpty()){
            refresh();
        }
        }catch (NullPointerException n){
            refresh();
        }*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String title) {
        if (mListener != null) {
            mListener.onFragmentInteraction(title);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentItems.OnFragmentInteractionListener) {
            mListener = (FragmentItems.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        model.reload();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }


}
