package com.example.sergi.wikibindingofisaac;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sergi on 9/12/17.
 */

public class IsaacAPI {

    private final String Base_URL = "https://isaac.jamesmcfadden.co.uk/api/v1";

    ArrayList<Item> items = new ArrayList<>();
    private String param;

    ArrayList<Item> peticionItems(String param){
        this.param = param;
        Uri builtUri = Uri.parse(Base_URL)
                .buildUpon()
                .appendPath(param)
                .build();
        String url = builtUri.toString();
        procesarJson(url);
        return items;
    }

    private ArrayList<Item> procesarJson(String url) {

        try{
            String JsonResponse = HttpUtils.get(url);
            JSONObject jsonres = new JSONObject((JsonResponse));
            JSONArray itemsArray = jsonres.getJSONArray("data");

            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject objeto = itemsArray.getJSONObject(i);
                Item item = new Item();
                item.setName(objeto.getString("name"));
                Log.d("name: ", objeto.getString( "name"));
                if (objeto.getString("sprite_url") != null) {
                    item.setImgURL(objeto.getString("sprite_url"));
                } else {
                    item.setImgURL("https://bindingofisaacrebirth.gamepedia.com/media/bindingofisaacrebirth.gamepedia.com/d/db/App_IamError.png?version=d63305e45642749a609699be641e1ac9");
                }
                item.setType(param);

                items.add(item);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return items;

    }


}
