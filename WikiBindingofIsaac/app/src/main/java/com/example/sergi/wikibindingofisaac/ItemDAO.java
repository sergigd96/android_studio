package com.example.sergi.wikibindingofisaac;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */
@Dao
public interface ItemDAO {

    @Query("select * from item where type = 'item'")
    LiveData<List<Item>> getItems();

    @Query("select * from item where type = 'monster'")
    LiveData<List<Item>> getMonsters();

    @Query("select * from item where type = 'boss'")
    LiveData<List<Item>> getBosses();

    @Query("select * from item where type = 'pickup'")
    LiveData<List<Item>> getPickups();

    @Insert
    void addItem(Item item);

    @Insert
    void addItems(List<Item> items);

    @Delete
    void deleteItem(Item item);

    @Query("delete from item where type = 'item'")
    void deleteItems();

    @Query("delete from item where type = 'monster'")
    void deleteMonsters();

    @Query("delete from item where type = 'pickup'")
    void deletePickups();

    @Query("delete from item where type = 'boss'")
    void deleteBosses();

}
