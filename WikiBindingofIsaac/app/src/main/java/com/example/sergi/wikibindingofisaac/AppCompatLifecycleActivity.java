package com.example.sergi.wikibindingofisaac;

import android.arch.lifecycle.LifecycleRegistry;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sergi on 24/11/17.
 */

public class AppCompatLifecycleActivity extends AppCompatActivity{

    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle(){
        return mRegistry;
    }

}
