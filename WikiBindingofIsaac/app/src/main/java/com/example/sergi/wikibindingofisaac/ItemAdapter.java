package com.example.sergi.wikibindingofisaac;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.example.sergi.wikibindingofisaac.databinding.LvItemsLayoutBinding;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */

public class ItemAdapter extends ArrayAdapter<Item>{

    public ItemAdapter(Context context, int resource, List<Item> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Item item= getItem(position);

        LvItemsLayoutBinding binding = null;

        if(convertView == null){
            LayoutInflater inflate = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflate, R.layout.lv_items_layout, parent,false);
        } else{
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.nameItem.setText(item.getName());
        Glide.with(getContext()).load(item.getImgURL()).into(binding.imageItem);
        return binding.getRoot();
    }

}
