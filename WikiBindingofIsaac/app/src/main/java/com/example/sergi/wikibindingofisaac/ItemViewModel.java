package com.example.sergi.wikibindingofisaac;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */

public class ItemViewModel extends AndroidViewModel{

    private final Application app;
    private final AppDatabase appDatabase;
    private final ItemDAO itemDAO;
    private MutableLiveData<Boolean> loading;
    private String param;

    public ItemViewModel(@NonNull Application application) {
        super(application);
        this.app = application;

        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.itemDAO = appDatabase.getItemDAO();
    }


    public LiveData<List<Item>> getItems(String param){
        this.param = param;
        switch (param){
            case "item":
                return itemDAO.getItems();
            case "pickup":
                return itemDAO.getPickups();
            case "monster":
                return itemDAO.getMonsters();
            case "boss":
                return itemDAO.getBosses();
        }
        return itemDAO.getItems();
    }

    public void reload(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }

        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Item>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Item> doInBackground(Void...voids) {
            IsaacAPI api = new IsaacAPI();
            ArrayList<Item> result;

            result = api.peticionItems(param);
            switch (param){
                case "item":
                    itemDAO.deleteItems();
                    itemDAO.addItems(result);
                    break;
                case "pickup":
                    itemDAO.deletePickups();
                    itemDAO.addItems(result);
                    break;
                case "monster":
                    itemDAO.deleteMonsters();
                    itemDAO.addItems(result);
                case "boss":
                    itemDAO.deleteBosses();
                    itemDAO.addItems(result);
                    break;
            }

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Item> items) {
            super.onPostExecute(items);
            loading.setValue(false);
        }

    }


}


