package com.example.sergi.wikibindingofisaac;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import java.io.Serializable;

/**
 * Created by sergi on 9/12/17.
 */
@Entity
public class Item implements Serializable{
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String imgURL;
    private String type;


    public Item(String name, String imgURL, String type) {
        this.name = name;
        this.imgURL = imgURL;
        this.type = type;
    }

    public Item() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
