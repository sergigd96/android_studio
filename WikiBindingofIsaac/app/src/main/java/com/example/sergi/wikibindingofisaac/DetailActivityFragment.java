package com.example.sergi.wikibindingofisaac;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.sergi.wikibindingofisaac.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    public DetailActivityFragment() {
    }

    private View view;
    private FragmentDetailBinding binding;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i!=null){
            Item item = (Item) i.getSerializableExtra("item");

            if(item != null){
                updateUi(item);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Item>() {
            @Override
            public void onChanged(@Nullable Item item) {
                updateUi(item);
            }
        });

        return view;
    }

    private void updateUi(Item item) {

        Glide.with(getContext()).load(item.getImgURL()).into(binding.imgItem);
        binding.nItem.setText(item.getName());

    }
}
