package com.example.sergi.cartas;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


/**
 * Created by sergi on 24/11/17.
 */

public class SharedViewModel extends ViewModel{

    private final MutableLiveData<Carta> selected = new MutableLiveData<Carta>();

    public LiveData<Carta> getSelected() {
        return selected;
    }

    public void select(Carta carta)
    {
        selected.setValue(carta);
    }





}
