package com.example.sergi.cartas;

import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import java.util.concurrent.ThreadLocalRandom;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sergi on 18/10/17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MagicCardsAPI {

    private final String Base_URL = "https://api.magicthegathering.io/v1";
    // Numero random para escojer página de api
    private int numPag = ThreadLocalRandom.current().nextInt(1, 349 + 1);
    ArrayList<Carta> cartas = new ArrayList<>();

    ArrayList<Carta> peticionCartas(String parameter, int par){
        if(par == 0){
            // Petición por nombre
            Uri builtUri = Uri.parse(Base_URL)
                .buildUpon()
                .appendPath("cards")
                .appendQueryParameter("name", parameter)
                .build();
            String url = builtUri.toString();
            procesarJson(url);
                }
        else if (par == 1){
            // Petición por general
            Uri builtUri = Uri.parse(Base_URL)
                    .buildUpon()
                    .appendPath("cards")
                    .appendQueryParameter("page", String.valueOf(numPag))
                    .build();
            String url = builtUri.toString();
            procesarJson(url);
        } else if (par == -1) {
            // Petición por tipo
            Uri builtUri = Uri.parse(Base_URL)
                    .buildUpon()
                    .appendPath("cards")
                    .appendQueryParameter("types", parameter)
                    .build();
            String url = builtUri.toString();
            procesarJson(url);
        }
        return cartas;
    }

    private ArrayList<Carta> procesarJson(String url){

        try {
            String JsonResponse = HttpUtils.get(url);
            JSONObject jsonres = new JSONObject((JsonResponse));
            JSONArray cartasArray = jsonres.getJSONArray("cards");

            for (int i = 0; i < cartasArray.length(); i++) {
                JSONObject objeto = cartasArray.getJSONObject(i);
                Carta carta = new Carta();
                carta.setName(objeto.getString("name"));
                carta.setManaCost(objeto.getString("cmc"));
                carta.setTipo(objeto.getString("type"));
                if (objeto.getString("text") != null) {
                    Log.d("Texto:" , objeto.getString("text"));
                    carta.setText(objeto.getString("text"));
                } else {
                    carta.setText("No tiene descripción");
                }
                //carta.setColors(objeto.getString("colors"));
                if (objeto.getString("imageUrl") != null) {
                    carta.setImageUrl(objeto.getString("imageUrl"));
                } else {
                    carta.setImageUrl("https://bindingofisaacrebirth.gamepedia.com/media/bindingofisaacrebirth.gamepedia.com/d/db/App_IamError.png?version=d63305e45642749a609699be641e1ac9");
                }

                cartas.add(carta);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cartas;
    }}
