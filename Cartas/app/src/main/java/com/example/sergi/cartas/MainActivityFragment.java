package com.example.sergi.cartas;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.example.sergi.cartas.databinding.FragmentMainBinding;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    private FragmentMainBinding binding;
    private SharedPreferences preferences;
    private CartasViewModel model;
    private SharedViewModel sharedModel;
    private ProgressDialog dialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.refresh){
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onStart() {
        super.onStart();
    }

    private void refresh()  {
        model.reload();
    }

    private ArrayList<Carta> items;
    private CartasAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();
        adapter = new CartasAdapter(
                getContext(),R.layout.lv_cartas,items
        );

        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        binding.lvCards.setAdapter(adapter);
        binding.lvCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Carta cartaPos = (Carta) adapterView.getItemAtPosition(i);

                if (!isTablet()) {
                    Intent intent = new Intent(getContext(),DetailActivity.class);
                    intent.putExtra("carta", cartaPos);
                    startActivity(intent);
                } else {
                    sharedModel.select(cartaPos);
                }
            }
        });

        model = ViewModelProviders.of(this).get(CartasViewModel.class);

        model.getCartas().observe(this, new Observer<List<Carta>>() {
            @Override
            public void onChanged(@Nullable List<Carta> cartas) {
                adapter.clear();
                adapter.addAll(cartas);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Cargando, espere por favor.");
        // Estilo "loading" en horizontal
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean seMuestra) {
                if(seMuestra){
                    dialog.show();
                    dialog.setProgress(100);

                } else {
                    dialog.dismiss();
                }
            }
        });
        refresh();
        return view;
    }

    boolean isTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }


}
