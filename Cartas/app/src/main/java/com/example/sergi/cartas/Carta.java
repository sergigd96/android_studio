package com.example.sergi.cartas;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by sergi on 18/10/17.
 */

@Entity
public class Carta implements Serializable {
    @PrimaryKey (autoGenerate = true)
    private int id;
    private String name;
    private String manaCost;
    private String colors;
    private String text;
    private String tipo;
    private String imageUrl;


    public Carta() {
    }

    public Carta(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public Carta(String name, String manaCost, String colors, String text, String tipo, String imageUrl) {
        this.name = name;
        this.manaCost = manaCost;
        this.colors = colors;
        this.text = text;
        this.tipo = tipo;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManaCost() {
        return manaCost;
    }

    public void setManaCost(String manaCost) {
        this.manaCost = manaCost;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
