package com.example.sergi.cartas;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;


import com.bumptech.glide.Glide;
import com.example.sergi.cartas.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;
    private FragmentDetailBinding binding;


    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         binding = FragmentDetailBinding.inflate(inflater);
         view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i!=null){
            Carta carta = (Carta) i.getSerializableExtra("carta");

            if(carta != null){
                updateUi(carta);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Carta>() {
            @Override
            public void onChanged(@Nullable Carta carta) {
                updateUi(carta);
            }
        });

        return view;
    }

    private void updateUi(Carta carta) {

        Glide.with(getContext()).load(carta.getImageUrl()).into(binding.imgDetailCard);
        binding.titleDetailCard.setText(carta.getName());
        binding.textDetailCard.setText("Descripción: \n"+carta.getText());
        binding.tipoDetailCard.setText("Tipo: "+carta.getTipo());
        binding.manaDetailCard.setText("Coste de Mana: "+carta.getManaCost());


    }
}
