package com.example.sergi.cartas;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergi on 15/11/17.
 */

public class CartasViewModel extends AndroidViewModel{

    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDAO cartaDao;
    private MutableLiveData<Boolean> loading;


    public CartasViewModel(@NonNull Application application) {
        super(application);
        this.app = application;

        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cartaDao = appDatabase.getCartaDAO();
    }

    public LiveData<List<Carta>> getCartas(){
        return cartaDao.getCartas();
    }

    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }

        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {
            MagicCardsAPI api = new MagicCardsAPI();
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
            String filtro = pref.getString("type_consulta", "Ninguno");
            String name = pref.getString("wName", "AWOL");
            String tipo = pref.getString("type","Artifact");
            ArrayList<Carta> result = null;

            // Controlador de tipo de consulta a la api
            if(filtro.equals("0")) {
                result = api.peticionCartas(name,0);
            } else if (filtro.equals("-1")){
                result = api.peticionCartas(tipo,-1);
            } else if (filtro.equals("1")){
                result = api.peticionCartas("",1);
            }

            cartaDao.deleteCartas();
            cartaDao.addCartas(result);

            return result;
        }


        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {
            super.onPostExecute(cartas);
            loading.setValue(false);
        }
    }
}
