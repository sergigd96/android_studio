package com.example.sergi.cartas;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sergi on 16/11/17.
 */
@Dao
public interface CartaDAO {

    @Query("select * from carta")
    LiveData<List<Carta>> getCartas();

    @Insert
    void addCarta(Carta carta);

    @Insert
    void addCartas(List<Carta> cartas);

    @Delete
    void deleteCarta(Carta carta);

    @Query("delete from carta")
    void deleteCartas();
}
