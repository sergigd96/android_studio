package com.example.sergi.cartas;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sergi.cartas.databinding.LvCartasBinding;

import java.util.List;

/**
 * Created by sergi on 29/10/17.
 */

public class CartasAdapter extends ArrayAdapter<Carta>{

    public CartasAdapter(Context context, int resource, List<Carta> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Carta carta= getItem(position);

        LvCartasBinding binding = null;

        if(convertView == null){
            LayoutInflater inflate = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflate, R.layout.lv_cartas, parent,false);
        } else{
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.tvCard.setText(carta.getName());
        Glide.with(getContext()).load(carta.getImageUrl()).into(binding.imageCard);
        return binding.getRoot();
    }
}
