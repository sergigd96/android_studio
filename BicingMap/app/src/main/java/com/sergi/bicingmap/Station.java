package com.sergi.bicingmap;

/**
 * Created by sergi on 26/01/18.
 */

public class Station {

    private String id, name;
    private String lat, lon, nearby;

    private Station() {
    }

    public Station(String id, String name, String lat, String lon, String nearby) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.nearby = nearby;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getNearby() {
        return nearby;
    }

    public void setNearby(String nearby) {
        this.nearby = nearby;
    }

    @Override
    public String toString() {
        return "Station{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", nearby=" + nearby +
                '}';
    }
}
