package com.sergi.isaacgo;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bumptech.glide.Glide;
import com.sergi.isaacgo.databinding.LvMonstersLayoutBinding;

import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */

public class MonsterAdapter extends ArrayAdapter<Monster>{

    public MonsterAdapter(Context context, int resource, List<Monster> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Monster monster = getItem(position);

        LvMonstersLayoutBinding binding = null;

        if(convertView == null){
            LayoutInflater inflate = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflate, R.layout.lv_monsters_layout, parent,false);
        } else{
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.nameMonster.setText(monster.getName());
        Glide.with(getContext()).load(monster.getSprite_url()).into(binding.imageMonster);
        if(monster.getMany() >0) {
            binding.catched.setText(("Catched: " + "Yes"));
        } else{
            binding.catched.setText(("Catched: " + "No"));
        }

        return binding.getRoot();
    }

}
