package com.sergi.isaacgo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by sergi on 9/12/17.
 */
@Entity
public class Monster implements Serializable{

    @PrimaryKey
    @NonNull
    private int id;

    private String name;
    private String sprite_url;
    private String url;
    private String lat;
    private String lon;
    private int many;

    @Ignore
    public Monster() {

    }

    public Monster(int id, String name, String sprite_url, String url, String lat, String lon) {
        this.id = id;
        this.name = name;
        this.sprite_url = sprite_url;
        this.url = url;
        this.lat = lat;
        this.lon = lon;
        this.many = 0;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSprite_url() {
        return sprite_url;
    }

    public String getUrl() {
        return url;
    }

    public int getMany() {
        return many;
    }

    public void setMany(int many) {
        this.many = many;
    }
}
