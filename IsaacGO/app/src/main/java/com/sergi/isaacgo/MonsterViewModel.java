package com.sergi.isaacgo;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */

public class MonsterViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final MonsterDAO monsterDAO;
    private MutableLiveData<Boolean> loading;
    private ArrayList<Monster> monsters = new ArrayList<>();
    private Monster monster;
    private int id;

    public MonsterViewModel(@NonNull Application application) {
        super(application);
        this.app = application;

        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.monsterDAO = appDatabase.getMonsterDAO();
    }


    public LiveData<List<Monster>> getMonsters(){
        return monsterDAO.getMonsters();
    }

    public void insertMonster(Monster monster){
        monsterDAO.addMonster(monster);
    }

    public void insertMonsters(ArrayList<Monster> monsters){
        this.monsters = monsters;
        insertMonstersInBD task = new insertMonstersInBD();
        task.execute();
    }

    public void getMonstersBD(){
        getMonstersFromBD task = new getMonstersFromBD();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }

        return loading;
    }

    public void catched(int idCapture) {
        this.id = idCapture;
        catchMonster task = new catchMonster();
        task.execute();
    }

    private class insertMonstersInBD extends AsyncTask<Void, Void, ArrayList<Monster>> {
        @Override
        protected ArrayList<Monster> doInBackground(Void...voids) {
            if (monsterDAO.getMonster() == null) {
                monsterDAO.addMonsters(monsters);
            }
            return monsters;

        }

    }

    private class getMonstersFromBD extends AsyncTask<Void, Void, LiveData<List<Monster>>> {
        @Override
        protected LiveData<List<Monster>> doInBackground(Void...voids) {

            return monsterDAO.getMonsters();


        }

    }

    private class catchMonster extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void...voids) {

            monsterDAO.catchMonster(id);
            return null;


        }

    }


}


