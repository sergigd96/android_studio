package com.sergi.isaacgo;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.Random;

import static android.content.ContentValues.TAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {


    private static final int RC_SIGN_IN = 123;
    private ArrayList<Monster> monsters = new ArrayList<>();
    private MapView map;
    private MonsterViewModel model;
    private MyLocationNewOverlay mLocationOverlay;
    private ScaleBarOverlay mScaleBarOverlay;
    private CompassOverlay mCompassOverlay;
    private IMapController mapController;
    private RotationGestureOverlay mRotationGestureOverlay;
    private RadiusMarkerClusterer stationMarkers;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private Monster monster;
    private MonsterDAO monsterDAO;
    private double latit = 41.394651;
    private double longi = 2.175962;
    private Multimedia multimedia;
    private Context ctx;
    Random r = new Random();
    int idCapture;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ctx = this.getContext();
        map = view.findViewById(R.id.map);
        mAuth = FirebaseAuth.getInstance();
        model = ViewModelProviders.of(this).get(MonsterViewModel.class);
        FloatingActionButton cam = view.findViewById(R.id.Cam);

        checkSignIn();
        model = ViewModelProviders.of(this).get(MonsterViewModel.class);
        initializeMap();
        setZoom();
        setOverlays();
        setRotate();
        setCompass();
        map.invalidate();

        cam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View view) {
                multimedia = new Multimedia(ctx);
                multimedia.dispatchTakePictureIntent();
                new makeFotoBack().execute();
            }
        });


        return view;
    }


    private void putMarkers() {
            setupMarkerOverlay();
            getStationInfo();
    }

    private void setupMarkerOverlay() {
        stationMarkers = new RadiusMarkerClusterer(this.getContext());

        Drawable clusterIconD = getResources().getDrawable(R.drawable.monster_gen);
        Bitmap clusterIcon = ((BitmapDrawable) clusterIconD).getBitmap();

        stationMarkers.setIcon(clusterIcon);
        stationMarkers.setRadius(100);

        stationMarkers.invalidate();
        map.getOverlays().add(stationMarkers);

    }

    private void getStationInfo() {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("data");

        ref.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    monster = child.getValue(Monster.class);

                    Marker marker = new Marker(map);
                    ArrayList pos = randomNum();
                    GeoPoint point = new GeoPoint(
                            Double.parseDouble(pos.get(0).toString()), Double.parseDouble(pos.get(1).toString())
                    );

                    monster.setLon(pos.get(0).toString());
                    monster.setLat(pos.get(1).toString());

                    monsters.add(monster);

                    marker.setPosition(point);

                    marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    marker.setIcon(getResources().getDrawable((R.drawable.monster_gen)));
                    marker.setTitle("Monstruo: " + monster.getName());
                    marker.setAlpha(0.8f);

                    stationMarkers.add(marker);
                }

                model.insertMonsters(monsters);
                stationMarkers.invalidate();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR: ", "The read failed: " + databaseError.getCode());
            }
        });

    }


    private void checkSignIn() {
        if (mAuth.getCurrentUser() == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            mGoogleSignInClient = GoogleSignIn.getClient(this.getContext(), gso);
            mAuth = FirebaseAuth.getInstance();
            signIn();
        } else {
            putMarkers();
        }
    }

    private void setCompass() {
        this.mCompassOverlay = new CompassOverlay(this.getContext(), new InternalCompassOrientationProvider(this.getContext()), map);
        this.mCompassOverlay.enableCompass();
        map.getOverlays().add(this.mCompassOverlay);
    }

    private void initializeMap() {
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setTilesScaledToDpi(true);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
    }


    private void setZoom() {
        mapController = map.getController();
        mapController.setZoom(14);
    }

    private void setOverlays() {

        final DisplayMetrics dm = getResources().getDisplayMetrics();
        mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(this.getContext()), map);
        mLocationOverlay.enableMyLocation();
        mLocationOverlay.runOnFirstFix(new Runnable() {
            public void run() {
                mapController.animateTo(mLocationOverlay.getMyLocation());
            }
        });

        mScaleBarOverlay = new ScaleBarOverlay(map);
        mScaleBarOverlay.setCentred(true);
        mScaleBarOverlay.setScaleBarOffset(dm.widthPixels / 2, 10);
        map.getOverlays().add(mLocationOverlay);
        map.getOverlays().add(this.mScaleBarOverlay);
    }

    private void setRotate() {
        mRotationGestureOverlay = new RotationGestureOverlay(this.getContext(), map);
        mRotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(this.mRotationGestureOverlay);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            putMarkers();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private ArrayList randomNum() {
        ArrayList pos = new ArrayList();
        double lat;
        double lon;
        if (mLocationOverlay.getMyLocation() != null) {
            lat = mLocationOverlay.getMyLocation().getLatitude() + (30 - 0) * r.nextDouble();
            lon = mLocationOverlay.getMyLocation().getLongitude() + (30 - 0) * r.nextDouble();
        } else {
            lat = latit + (0 - 0.05) * r.nextDouble();
            lon = longi + (0 - 0.05) * r.nextDouble();
        }

        pos.add(lat);
        pos.add(lon);

        return pos;
    }


    private class makeFotoBack extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            idCapture = r.nextInt(100 - 0);
            Log.d(TAG, "Id del capturado: "+idCapture);
            model.catched(idCapture);
            return null;
        }


    }
}
